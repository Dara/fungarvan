@extends('app')

@section('content')
    <script src="https://cdn.socket.io/socket.io-1.3.4.js"></script>
    <script>
        window.onload = function(){
            var socket = io.connect('{{ url('/') }}', { path: '/realtime/socket.io'});
            socket.on('news', function (data) {
                console.log('I have some news');
                console.log(data);
                socket.emit('my other event', { my: 'data'});
            });
        };
    </script>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Home</div>

                    <div class="panel-body">
                        You are logged in!
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
