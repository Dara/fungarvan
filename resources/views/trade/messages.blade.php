@extends('app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="table-responsive">

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <h3>Trade Messages</h3>
                    <table class="table table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Time Placed</th>
                            <th>Currency From</th>
                            <th>Amount Sold</th>
                            <th>Currency To</th>
                            <th>Amount Bought</th>
                            <th>Rate</th>
                            <th>Country of Origin</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($tradeMessages as $message)
                            <tr>
                                <td>{{{$message->timePlaced}}} ({{$message->timePlaced->diffForHumans()}})</td>
                                <td>{{$message->currencyFrom}}</td>
                                <td>{{$message->amountSell}}</td>
                                <td>{{$message->currencyTo}}</td>
                                <td>{{$message->amountBuy}}</td>
                                <td>{{$message->rate}}</td>
                                <td>{{$message->originatingCountry}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {!! $tradeMessages->render() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
