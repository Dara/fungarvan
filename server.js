var app = require('http').createServer(handler);
//any request that comes in on the /realtime path
//is routed to the node server running on port 5000
//so need to make sure that the socket.io file is served from there too
var io = require('socket.io')(app, { path: '/realtime/socket.io'});
var fs = require('fs');
//use redis as the interface between php and the node server
var redis = require('redis');
var redisClient = redis.createClient();
redisClient.subscribe('tradeMessage.added');

app.listen(5000);

function handler(req, res) {
}

io.on('connection', function (socket) {
    socket.emit('news', { hello: 'world' });
    socket.on('my other event', function (data) {
        console.log(data);
    });
    redisClient.on('message', function(channel, message) {
        socket.emit('news', message);
    });
});