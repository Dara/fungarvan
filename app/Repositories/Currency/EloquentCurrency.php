<?php

namespace Fungarvan\Repositories\Currency;


use Fungarvan\CurrenciesBought;
use Fungarvan\Currency;
use Fungarvan\CurrencyBought;
use Fungarvan\CurrencySold;

class EloquentCurrency implements CurrencyInterface{
    /**
     * @var Model
     */
    protected $currency;

    /**
     * Slots in the bought and sold database. The currencies
     * are shared across multiple slots to prevent
     * a bottle-neck in the database when incrementing
     * the figures
     * @var int the number of slots for each currency
     */
    protected $slots = 10;

    /**
     * @param Currency $currency
     */
    public function __construct(Currency $currency)

    {
        $this->currency = $currency;
    }

    /**
     * Retrieve the currency by ISO Code
     *
     * @param string $iso the three letter currency code
     * @return standard object containing the currency information
     */
    public function byISO($iso)
    {
        return $this->currency
            ->where('currencyISO', $iso)->first();
    }

    /**
     * @param string $iso Three character currency iso
     * @param $amount
     * @return mixed
     */
    public function sellCurrency($iso, $amount)
    {
        $slot = rand(1,10);
        $currencySold = CurrencySold::firstOrNew(['currencyISO' => $iso, 'slot' => $slot]);
        $currencySold->amount += $amount;
        $currencySold->save();
    }

    /**
     * @param string $iso Three character currency iso
     * @param $amount
     * @return mixed
     */
    public function buyCurrency($iso, $amount)
    {
        $slot = rand(1,10);
        $currencySold = CurrencyBought::firstOrNew(['currencyISO' => $iso, 'slot' => $slot]);
        $currencySold->amount += $amount;
        $currencySold->save();
    }
}