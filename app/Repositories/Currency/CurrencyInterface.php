<?php

namespace Fungarvan\Repositories\Currency;


interface CurrencyInterface {

    /**
     * Retrieve the currency by ISO Code
     *
     * @param string $iso the three letter currency code
     * @return standard object containing the currency information
     */
    public function byISO($iso);

    /**
     * @param string $iso Three character currency iso
     * @param $amount
     * @return mixed
     */
    public function sellCurrency($iso, $amount);

    /**
     * @param string $iso Three character currency iso abbre
     * @param $amount
     * @return mixed
     */
    public function buyCurrency($iso, $amount);

}