<?php namespace Fungarvan;

use Illuminate\Database\Eloquent\Model;

class CurrencySold extends Model{

    protected $table = 'currencies_sold';
    public $timestamps = false;
    protected $fillable = [
        'currencyISO', 'slot', 'amount'
    ];

}
