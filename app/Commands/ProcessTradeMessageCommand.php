<?php namespace Fungarvan\Commands;

use Fungarvan\Repositories\Currency\CurrencyInterface;
use Fungarvan\TradeMessage;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

/**
 * Class ProcessTradeMessageCommand
 * @package Fungarvan\Commands
 */
class ProcessTradeMessageCommand extends Command implements ShouldBeQueued, SelfHandling{

    use InteractsWithQueue, SerializesModels;
    /**
     * @var TradeMessage
     */
    public $tradeMessage;

    /**
     * Create a new command instance.
     *
     * @param TradeMessage $tradeMessage
     */
	public function __construct(TradeMessage $tradeMessage)
	{
        $this->tradeMessage = $tradeMessage;
    }

    /**
     * Execute the command.
     *
     * @param CurrencyInterface $currency
     */
	public function handle(CurrencyInterface $currency)
	{
        //save the amount of currency bought and sold to separate tables for reporting
        $currency->sellCurrency(
            $this->tradeMessage['currencyFrom'],
            $this->tradeMessage['amountSell']
        );

        $currency->buyCurrency(
            $this->tradeMessage['currencyTo'],
            $this->tradeMessage['amountBuy']
        );

        Redis::connection();
        Redis::publish('tradeMessage.added', $this->tradeMessage);
	}

}
