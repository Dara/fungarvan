<?php namespace Fungarvan;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TradeMessage
 * @package Fungarvan
 */
class TradeMessage extends Model {

    /**
     * Override the name of the "created at" column.
     * We want it to be camel case
     *
     * @var string
     */
    const CREATED_AT = 'createdAt';

    /**
     * Override the name of the "updated at" column.
     * We want it to be camel case
     *
     * @var string
     */
    const UPDATED_AT = 'updatedAt';

    /**
     * allow mass assignment of the following fields
     */
    protected $fillable = [
        'userId', 'currencyFrom', 'currencyTo', 'amountSell', 'amountBuy', 'rate', 'timePlaced', 'originatingCountry'
    ];


    /**
     * cast the data to it's correct type
     */
    protected $casts = [
        'userID' => 'integer',
        'amountSell' => 'float',
        'amountBuy' => 'float',
        'rate' => 'float',
    ];

    /*
     * want to add carbon functionality to our dates
     */
    protected $dates = ['timePlaced'];

    /*
     * make sure to convert the date from the type that is passed in
     */
    protected function setTimePlacedAttribute($date)
    {
        $this->attributes['timePlaced'] = Carbon::parse($date);
    }

}
