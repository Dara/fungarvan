<?php namespace Fungarvan\Http\Controllers;

use Fungarvan\Commands\ProcessTradeMessageCommand;
use Fungarvan\TradeMessage;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class TradeMessageController extends Controller {

    protected $validationRules = [
        'userId' => 'required|integer',
        'currencyFrom' => 'required|string|size:3',
        'currencyTo' => 'required|string|size:3',
        'amountSell' => 'required|numeric',
        'amountBuy' => 'required|numeric',
        'rate' => 'required|numeric',
        'timePlaced' => 'required|date',
        'originatingCountry' => 'required|string|size:2'
    ];

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $tradeMessages = TradeMessage::latest('timePlaced')->paginate(15);
        if(!$request->isJson()) {
            return view('trade.messages')->with('tradeMessages', $tradeMessages);
        }
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

    /**
     * Store a newly created trade_message.
     *
     * @param Request $request
     * @return Response
     */
	public function store(Request $request)
	{
        if($request->isJson())
        {
            $tradeMessage = TradeMessage::create($request->all());
            $v = Validator::make($tradeMessage->toArray(), $this->validationRules);
            if($v->fails())
            {
                //send back the errors with a 422
                return response()->json(['error'=>['message'=>$v->errors(),'code'=>Response::HTTP_UNPROCESSABLE_ENTITY]],Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $tradeMessage->save();
            $tradeMessageId = $tradeMessage->id;
            //Do some processing of the trade message later (eg with a queue)
            $this->dispatch(new ProcessTradeMessageCommand($tradeMessage));
            return response()->json(['data'=>['id'=>$tradeMessageId]],Response::HTTP_ACCEPTED);

        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
