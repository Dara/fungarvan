<?php namespace Fungarvan;

use Illuminate\Database\Eloquent\Model;

class CurrencyBought extends Model{

    protected $table = 'currencies_bought';
    public $timestamps = false;
    protected $fillable = [
        'currencyISO', 'slot', 'amount'
    ];

}
