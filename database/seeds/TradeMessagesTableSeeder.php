<?php
use Fungarvan\Country;
use Fungarvan\Currency;
use Fungarvan\Faker\Provider\TradeMessage as FakeTradeMessage;
use Fungarvan\TradeMessage;
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: Dara
 * Date: 25/03/15
 * Time: 08:55
 */

class TradeMessagesTableSeeder extends Seeder{

    private $numberOfMessages = 200;

    public function run()
    {
        DB::table('trade_messages')->delete();
        //use faker lib to seed the database with some
        //data....we will use a random currency each time
        //with a random number and random exchange rates
        $faker = Faker\Factory::create();
        $faker->addProvider(new FakeTradeMessage($faker));
        $currencies = Currency::all(['currencyISO'])->toArray();
        $countries = Country::all(['countryISO'])->toArray();
        $itemCount = $this->numberOfMessages;
        while($itemCount>0)
        {
            $tradeCurrencies = $faker->randomElements($currencies, 2);
            //rate is a random number, not very scientific or profitable!
            $rate = $faker->randomFloat(4,.1,10);
            $amountSell = $faker->randomFloat(2,20,2000);
            $amountBuy = $amountSell * $rate;

            $tradeMessage = TradeMessage::create([
                    'userId' => $faker->userId,
                    'currencyFrom' => $tradeCurrencies[0]['currencyISO'],
                    'currencyTo' => $tradeCurrencies[1]['currencyISO'],
                    'amountSell' => $amountSell,
                    'amountBuy' => $amountBuy,
                    'rate' => $rate,
                    'timePlaced' => $faker->dateTimeThisMonth(),
                    'originatingCountry' => $faker->randomElement($countries)['countryISO']
                ]
            );
            $tradeMessage->save();
            $itemCount--;
        }
    }

}
