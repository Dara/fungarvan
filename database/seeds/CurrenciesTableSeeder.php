<?php
use Fungarvan\Currency;
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: Dara
 * Date: 25/03/15
 * Time: 08:55
 */

class CurrenciesTableSeeder extends Seeder{

    public function run()
    {
        DB::table('currencies')->delete();

        $currencies = [
            ['currencyISO' => 'AED', 'currencyTitle' => 'UAE Dirham'],
            ['currencyISO' => 'AUD', 'currencyTitle' => 'Australian Dollar'],
            ['currencyISO' => 'BGN', 'currencyTitle' => 'Bulgarian Lev'],
            ['currencyISO' => 'CAD', 'currencyTitle' => 'Canadian Dollar'],
            ['currencyISO' => 'CHF', 'currencyTitle' => 'Swiss Franc'],
            ['currencyISO' => 'CZK', 'currencyTitle' => 'Czech Koruna'],
            ['currencyISO' => 'DKK', 'currencyTitle' => 'Danish Krone'],
            ['currencyISO' => 'EUR', 'currencyTitle' => 'Euro'],
            ['currencyISO' => 'GBP', 'currencyTitle' => 'Pound Sterling'],
            ['currencyISO' => 'HKD', 'currencyTitle' => 'Hong Kong Dollar'],
            ['currencyISO' => 'HUF', 'currencyTitle' => 'Hungarian Forint'],
            ['currencyISO' => 'ILS', 'currencyTitle' => 'Israeli New Shekel'],
            ['currencyISO' => 'JPY', 'currencyTitle' => 'Japanese Yen'],
            ['currencyISO' => 'NOK', 'currencyTitle' => 'Norweigan Kroner'],
            ['currencyISO' => 'NZD', 'currencyTitle' => 'New Zealand Dollar'],
            ['currencyISO' => 'PLN', 'currencyTitle' => 'Polish Zloty'],
            ['currencyISO' => 'RON', 'currencyTitle' => 'Romanian New Leu'],
            ['currencyISO' => 'SEK', 'currencyTitle' => 'Swedish Krona'],
            ['currencyISO' => 'SGD', 'currencyTitle' => 'Singapore Dollar'],
            ['currencyISO' => 'THB', 'currencyTitle' => 'Thai Baht'],
            ['currencyISO' => 'USD', 'currencyTitle' => 'United States Dollar'],
            ['currencyISO' => 'ZAR', 'currencyTitle' => 'South African Rand']
        ];

        foreach($currencies as $currency)
        {
            Currency::create($currency);
        }

    }

}
