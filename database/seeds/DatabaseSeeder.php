<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('CurrenciesTableSeeder');
        $this->call('CountriesTableSeeder');
        //database seeder only used for dev
        //I installed JMeter and use that to
        //test the endpoint and seed data now.
//        $this->call('TradeMessagesTableSeeder');
	}

}
