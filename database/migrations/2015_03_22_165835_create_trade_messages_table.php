<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTradeMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('trade_messages', function(Blueprint $table)
		{
			$table->bigInteger('id',true,true);
			$table->bigInteger('userId',false,true);
			$table->char('currencyFrom',3);
			$table->char('currencyTo',3);
			$table->decimal('amountSell',8,2);
			$table->decimal('amountBuy',8,2);
			$table->decimal('rate',6,4);
			$table->dateTime('timePlaced');
			$table->char('originatingCountry',2);
            $table->boolean('processed')->default(false);
            $table->timestamp('createdAt');
            $table->timestamp('updatedAt');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('trade_messages');
	}

}
