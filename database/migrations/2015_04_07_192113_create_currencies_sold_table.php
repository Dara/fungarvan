<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrenciesSoldTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('currencies_sold', function(Blueprint $table)
		{
            $table->increments('id');
            $table->char('currencyISO',3);
            $table->tinyInteger('slot');
			$table->decimal('amount',8,2);
            $table->unique(['currencyISO','slot']);
            //only want to allow currencies that currency fair trades in
            $table->foreign('currencyISO')
                ->references('currencyISO')->on('currencies');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('currencies_sold');
	}

}
